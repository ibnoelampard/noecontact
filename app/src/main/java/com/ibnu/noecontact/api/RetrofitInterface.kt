package com.ibnu.noecontact.api

import com.ibnu.noecontact.pojo.BaseResponse
import com.ibnu.noecontact.pojo.Contact
import com.ibnu.noecontact.pojo.ContactResponse
import retrofit2.Call
import retrofit2.http.*

interface RetrofitInterface {

    @GET("/contact")
    fun getAllContact() : Call<ContactResponse>

    @GET("/contact/{id}")
    fun getContactById(@Path("id") id: String) : Call<ContactResponse>

    @POST("/contact")
    fun saveContact(@Body bodyRequest: Contact) : Call<BaseResponse>

    @PUT("/contact/{id}")
    fun updateContact(@Path("id") id: String) : Call<BaseResponse>

    @DELETE("/contact")
    fun deleteContact(@Body bodyRequest: Contact) : Call<BaseResponse>
}