package com.ibnu.noecontact.pojo

import com.google.gson.annotations.SerializedName

open class BaseResponse{
    @SerializedName("message")
    var message: String? = null
}

class ContactResponse: BaseResponse(){
    @SerializedName("data")
    var data:ArrayList<Contact>? = null
}

class Contact {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("lastName")
    var lastName: String? = null

    @SerializedName("age")
    var age: Int? = 0

    @SerializedName("photo")
    var photo: String? = null
}